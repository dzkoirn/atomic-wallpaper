package by.dzkoirn.atomic

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.graphics.g3d.ModelBatch

class AtomicWallpaper : ApplicationAdapter() {

    private lateinit var batch: SpriteBatch
    private lateinit var img: Texture
    private lateinit var modelBatch: ModelBatch

    private lateinit var cam: PerspectiveCamera
    private lateinit var model: Model
    private lateinit var modelInstance: ModelInstance

    override fun create() {
        batch = SpriteBatch()
        img = Texture("badlogic.jpg")
        modelBatch = ModelBatch()

        cam = PerspectiveCamera(67f, Gdx.graphics.getWidth().toFloat(), Gdx.graphics.getHeight().toFloat());
        cam.position.set(10f, 10f, 10f);
        cam.lookAt(0f, 0f, 0f);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        val modelBuilder = ModelBuilder()
        model = modelBuilder.createBox(5f, 5f, 5f,
                Material(ColorAttribute.createDiffuse(Color.GREEN)),
                (VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal).toLong())
        modelInstance = ModelInstance(model)
    }

    override fun render() {
//        Gdx.gl.glClearColor(1f, 0f, 0f, 1f)
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
//        batch.begin()
//        batch.draw(img, 0f, 0f)
//        batch.end()

        Gdx.gl.glViewport(0, 0, Gdx.graphics.width, Gdx.graphics.height)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        modelBatch.begin(cam)
        modelBatch.render(modelInstance)
        modelBatch.end()
    }

    override fun dispose() {
        batch.dispose()
        img.dispose()
        modelBatch.dispose()
        model.dispose()
    }
}
